use std::path::PathBuf;

use clap::Parser;

#[derive(Clone, Debug, Parser)]
pub struct ClientSettings {
    /// Project path
    #[arg(long = "project-path", short = 'P')]
    path: Option<PathBuf>,

    /// Project
    project: Option<String>,
}
